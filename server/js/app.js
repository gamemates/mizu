var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mongodb = require('mongodb');
var port = 3000;
var MongoClient = mongodb.MongoClient;
var db_url = 'mongodb://localhost:27017/roguedb';
var path = require('path');

//Set route for client folder
app.use(express.static(path.join(__dirname, '../../client')));

//Send the HTML in the client folder
app.get('/', function(req, res){
	res.sendFile('index.html');
});

//Make sure server running
http.listen(port, function(){
	console.log('listening on *:' + port);
});

//Check for users connected
io.on('connection', function(socket){
	console.log('a user connected');
});

// Use connect method to connect to the Server
MongoClient.connect(db_url, function (err, db) {
  if (err) {
    console.log('Unable to connect to the mongoDB server. Error:', err);
  } else {
    console.log('Connection established to', db_url);

    db.close();
  }
});